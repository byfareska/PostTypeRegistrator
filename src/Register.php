<?php
/**
 * Created by PhpStorm.
 * User: eska
 * Date: 12.09.18
 * Time: 20:43
 */

namespace KarolSzarafinowski\PostTypeRegistrator;

class Register
{
    /**
     * @var string
     */
    private $postType;

    /**
     * @var string general name for the post type, usually plural. The same and overridden by $post_type_object->label. Default is Posts/Pages
     */
    private $name;

    /**
     * @var string name for one object of this post type. Default is Post/Page
     */
    private $singularName;

    /**
     * @var string the add new text.
     */
    private $addNew;

    /**
     * @var string Default is Add New Post/Add New Page.
     */
    private $addNewItem;

    /**
     * @var string Default is Edit Post/Edit Page.
     */
    private $editItem;

    /**
     * @var string Default is New Post/New Page.
     */
    private $newItem;

    /**
     * @var string Default is View Post/View Page.
     */
    private $viewItem;

    /**
     * @var string Label for viewing post type archives. Default is 'View Posts' / 'View Pages'.
     */
    private $viewItems;

    /**
     * @var string Default is Search Posts/Search Pages.
     */
    private $searchItems;

    /**
     * @var string Default is No posts found/No pages found.
     */
    private $notFound;

    /**
     * @var string Default is No posts found in Trash/No pages found in Trash.
     */
    private $notFoundInTrash;

    /**
     * @var string This string isn't used on non-hierarchical types. In hierarchical ones the default is 'Parent Page:'.
     */
    private $parentItemColon;

    /**
     * @var string String for the submenu. Default is All Posts/All Pages.
     */
    private $allItems;

    /**
     * @var string String for use with archives in nav menus. Default is Post Archives/Page Archives.
     */
    private $archives;

    /**
     * @var string Label for the attributes meta box. Default is 'Post Attributes' / 'Page Attributes'.
     */
    private $attributes;

    /**
     * @var string String for the media frame button. Default is Insert into post/Insert into page.
     */
    private $insertIntoItem;

    /**
     * @var string String for the media frame filter. Default is Uploaded to this post/Uploaded to this page.
     */
    private $uploadedToThisItem;

    /**
     * @var string Default is Featured Image.
     */
    private $featuredImage;

    /**
     * @var string Default is Set featured image.
     */
    private $setFeaturedImage;

    /**
     * @var string Default is Remove featured image.
     */
    private $removeFeaturedImage;

    /**
     * @var string Default is Use as featured image.
     */
    private $useFeaturedImage;

    /**
     * @var string Default is the same as `name`.
     */
    private $menuName;

    /**
     * @var string String for the table views hidden heading.
     */
    private $filterItemsList;

    /**
     * @var string String for the table pagination hidden heading.
     */
    private $itemsListNavigation;

    /**
     * @var string String for the table hidden heading.
     */
    private $itemsList;

    /**
     * @var string String for use in New in Admin menu bar. Default is the same as `singular_name`.
     */
    private $nameAdminBar;

    /**
     * @var bool Controls how the type is visible to authors (show_in_nav_menus, show_ui) and readers (exclude_from_search, publicly_queryable).
     */
    private $public = true;

    /**
     * @var bool Whether to exclude posts with this post type from front end search results. Default: value of the opposite of public argument
     */
    private $excludeFromSearch;

    /**
     * @var bool Whether queries can be performed on the front end as part of parse_request(). Default: value of public argument
     */
    private $publiclyQueryable;

    /**
     * @var bool Whether to generate a default UI for managing this post type in the admin.
     */
    private $showUi;

    /**
     * @var bool Whether post_type is available for selection in navigation menus.
     */
    private $showInNavMenus;

    /**
     * @var bool|string Where to show the post type in the admin menu. show_ui must be true.
     */
    private $showInMenu;
    /**
     * @var bool Whether to make this post type available in the WordPress admin bar
     */
    private $showInAdminBar;
    /**
     * @var integer The position in the menu order the post type should appear. show_in_menu must be true.
     *
     * Default: null - defaults to below Comments
     *
     * 5 - below Posts
     * 10 - below Media
     * 15 - below Links
     * 20 - below Pages
     * 25 - below comments
     * 60 - below first separator
     * 65 - below Plugins
     * 70 - below Users
     * 75 - below Tools
     * 80 - below Settings
     * 100 - below second separator
     */
    private $menuPosition;

    /**
     * @var string The url to the icon to be used for this menu or the name of the icon from the iconfont [1]
     */
    private $menuIcon;

    /**
     * @var string|array
     */
    private $capabilityType;

    /**
     * @var array
     */
    private $capabilities;

    /**
     * @var bool
     */
    private $mapMetaCap;

    /**
     * @var bool
     */
    private $hierarchical;

    /**
     * @var bool|array
     */
    private $supports;

    /**
     * @var callable
     */
    private $registerMetaBoxCb;

    /**
     * @var array
     */
    private $taxonomies;

    /**
     * @var boolean|string
     */
    private $hasArchive;

    /**
     * @var bool|array
     */
    private $rewrite;


    private $permalinkEpmask;
    /**
     * @var bool|string
     */
    private $queryVar;

    /**
     * @var bool
     */
    private $canExport;

    /**
     * @var bool
     */
    private $deleteWithUser;

    /**
     * @var bool
     */
    private $showInRest;

    /**
     * @var string
     */
    private $restBase;

    /**
     * @var string
     */
    private $restControllerClass;

    public function __construct($postType)
    {
        $this->postType = $postType;
    }

    public static function factory($postType): Register
    {
        return new Register($postType);
    }

    /**
     * @param string $postType
     */
    public function setPostType(string $postType): void
    {
        $this->postType = $postType;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $singularName
     */
    public function setSingularName(string $singularName): void
    {
        $this->singularName = $singularName;
    }

    /**
     * @param string $addNew
     */
    public function setAddNew(string $addNew): void
    {
        $this->addNew = $addNew;
    }

    /**
     * @param string $addNewItem
     */
    public function setAddNewItem(string $addNewItem): void
    {
        $this->addNewItem = $addNewItem;
    }

    /**
     * @param string $editItem
     */
    public function setEditItem(string $editItem): void
    {
        $this->editItem = $editItem;
    }

    /**
     * @param string $newItem
     */
    public function setNewItem(string $newItem): void
    {
        $this->newItem = $newItem;
    }

    /**
     * @param string $viewItem
     */
    public function setViewItem(string $viewItem): void
    {
        $this->viewItem = $viewItem;
    }

    /**
     * @param string $viewItems
     */
    public function setViewItems(string $viewItems): void
    {
        $this->viewItems = $viewItems;
    }

    /**
     * @param string $searchItems
     */
    public function setSearchItems(string $searchItems): void
    {
        $this->searchItems = $searchItems;
    }

    /**
     * @param string $notFound
     */
    public function setNotFound(string $notFound): void
    {
        $this->notFound = $notFound;
    }

    /**
     * @param string $notFoundInTrash
     */
    public function setNotFoundInTrash(string $notFoundInTrash): void
    {
        $this->notFoundInTrash = $notFoundInTrash;
    }

    /**
     * @param string $parentItemColon
     */
    public function setParentItemColon(string $parentItemColon): void
    {
        $this->parentItemColon = $parentItemColon;
    }

    /**
     * @param string $allItems
     */
    public function setAllItems(string $allItems): void
    {
        $this->allItems = $allItems;
    }

    /**
     * @param string $archives
     */
    public function setArchives(string $archives): void
    {
        $this->archives = $archives;
    }

    /**
     * @param string $attributes
     */
    public function setAttributes(string $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @param string $insertIntoItem
     */
    public function setInsertIntoItem(string $insertIntoItem): void
    {
        $this->insertIntoItem = $insertIntoItem;
    }

    /**
     * @param string $uploadedToThisItem
     */
    public function setUploadedToThisItem(string $uploadedToThisItem): void
    {
        $this->uploadedToThisItem = $uploadedToThisItem;
    }

    /**
     * @param string $featuredImage
     */
    public function setFeaturedImage(string $featuredImage): void
    {
        $this->featuredImage = $featuredImage;
    }

    /**
     * @param string $setFeaturedImage
     */
    public function setSetFeaturedImage(string $setFeaturedImage): void
    {
        $this->setFeaturedImage = $setFeaturedImage;
    }

    /**
     * @param string $removeFeaturedImage
     */
    public function setRemoveFeaturedImage(string $removeFeaturedImage): void
    {
        $this->removeFeaturedImage = $removeFeaturedImage;
    }

    /**
     * @param string $useFeaturedImage
     */
    public function setUseFeaturedImage(string $useFeaturedImage): void
    {
        $this->useFeaturedImage = $useFeaturedImage;
    }

    /**
     * @param string $menuName
     */
    public function setMenuName(string $menuName): void
    {
        $this->menuName = $menuName;
    }

    /**
     * @param string $filterItemsList
     */
    public function setFilterItemsList(string $filterItemsList): void
    {
        $this->filterItemsList = $filterItemsList;
    }

    /**
     * @param string $itemsListNavigation
     */
    public function setItemsListNavigation(string $itemsListNavigation): void
    {
        $this->itemsListNavigation = $itemsListNavigation;
    }

    /**
     * @param string $itemsList
     */
    public function setItemsList(string $itemsList): void
    {
        $this->itemsList = $itemsList;
    }

    /**
     * @param string $nameAdminBar
     */
    public function setNameAdminBar(string $nameAdminBar): void
    {
        $this->nameAdminBar = $nameAdminBar;
    }

    /**
     * @param bool $public
     */
    public function setPublic(bool $public): void
    {
        $this->public = $public;
    }

    /**
     * @param bool $excludeFromSearch
     */
    public function setExcludeFromSearch(bool $excludeFromSearch): void
    {
        $this->excludeFromSearch = $excludeFromSearch;
    }

    /**
     * @param bool $publiclyQueryable
     */
    public function setPubliclyQueryable(bool $publiclyQueryable): void
    {
        $this->publiclyQueryable = $publiclyQueryable;
    }

    /**
     * @param bool $showUi
     */
    public function setShowUi(bool $showUi): void
    {
        $this->showUi = $showUi;
    }

    /**
     * @param bool $showInNavMenus
     */
    public function setShowInNavMenus(bool $showInNavMenus): void
    {
        $this->showInNavMenus = $showInNavMenus;
    }

    /**
     * @param bool|string $showInMenu
     */
    public function setShowInMenu($showInMenu): void
    {
        $this->showInMenu = $showInMenu;
    }

    /**
     * @param bool $showInAdminBar
     */
    public function setShowInAdminBar(bool $showInAdminBar): void
    {
        $this->showInAdminBar = $showInAdminBar;
    }

    /**
     * @param int $menuPosition
     */
    public function setMenuPosition(int $menuPosition): void
    {
        $this->menuPosition = $menuPosition;
    }

    /**
     * @param string $menuIcon
     */
    public function setMenuIcon(string $menuIcon): void
    {
        $this->menuIcon = $menuIcon;
    }

    /**
     * @param array|string $capabilityType
     */
    public function setCapabilityType($capabilityType): void
    {
        $this->capabilityType = $capabilityType;
    }

    /**
     * @param array $capabilities
     */
    public function setCapabilities(array $capabilities): void
    {
        $this->capabilities = $capabilities;
    }

    /**
     * @param bool $mapMetaCap
     */
    public function setMapMetaCap(bool $mapMetaCap): void
    {
        $this->mapMetaCap = $mapMetaCap;
    }

    /**
     * @param bool $hierarchical
     */
    public function setHierarchical(bool $hierarchical): void
    {
        $this->hierarchical = $hierarchical;
    }

    /**
     * @param array|bool $supports
     */
    public function setSupports($supports): void
    {
        $this->supports = $supports;
    }

    /**
     * @param callable $registerMetaBoxCb
     */
    public function setRegisterMetaBoxCb(callable $registerMetaBoxCb): void
    {
        $this->registerMetaBoxCb = $registerMetaBoxCb;
    }

    /**
     * @param array $taxonomies
     */
    public function setTaxonomies(array $taxonomies): void
    {
        $this->taxonomies = $taxonomies;
    }

    /**
     * @param bool|string $hasArchive
     */
    public function setHasArchive($hasArchive): void
    {
        $this->hasArchive = $hasArchive;
    }

    /**
     * @param array|bool $rewrite
     */
    public function setRewrite($rewrite): void
    {
        $this->rewrite = $rewrite;
    }

    /**
     * @param mixed $permalinkEpmask
     */
    public function setPermalinkEpmask($permalinkEpmask): void
    {
        $this->permalinkEpmask = $permalinkEpmask;
    }

    /**
     * @param bool|string $queryVar
     */
    public function setQueryVar($queryVar): void
    {
        $this->queryVar = $queryVar;
    }

    /**
     * @param bool $canExport
     */
    public function setCanExport(bool $canExport): void
    {
        $this->canExport = $canExport;
    }

    /**
     * @param bool $deleteWithUser
     */
    public function setDeleteWithUser(bool $deleteWithUser): void
    {
        $this->deleteWithUser = $deleteWithUser;
    }

    /**
     * @param bool $showInRest
     */
    public function setShowInRest(bool $showInRest): void
    {
        $this->showInRest = $showInRest;
    }

    /**
     * @param string $restBase
     */
    public function setRestBase(string $restBase): void
    {
        $this->restBase = $restBase;
    }

    /**
     * @param string $restControllerClass
     */
    public function setRestControllerClass(string $restControllerClass): void
    {
        $this->restControllerClass = $restControllerClass;
    }

    public function register(): void
    {
        register_post_type($this->postType, $this->optionalOptions());
    }

    private function optionalOptions(): array
    {
        $opt = [
            'labels' => $this->labelOptions(),
            'public' => $this->public
        ];

        if (isSet($this->excludeFromSearch)) $opt['exclude_from_search'] = $this->excludeFromSearch;
        if (isSet($this->publiclyQueryable)) $opt['publicly_queryable'] = $this->publiclyQueryable;
        if (isSet($this->showUi)) $opt['show_ui'] = $this->showUi;
        if (isSet($this->showInNavMenus)) $opt['show_in_nav_menus'] = $this->showInNavMenus;
        if (isSet($this->showInMenu)) $opt['show_in_menu'] = $this->showInMenu;
        if (isSet($this->showInAdminBar)) $opt['show_in_admin_bar'] = $this->showInAdminBar;
        if (isSet($this->menuPosition)) $opt['menu_position'] = $this->menuPosition;
        if (isSet($this->menuIcon)) $opt['menu_icon'] = $this->menuIcon;
        if (isSet($this->capabilityType)) $opt['capability_type'] = $this->capabilityType;
        if (isSet($this->capabilities)) $opt['capabilities'] = $this->capabilities;
        if (isSet($this->mapMetaCap)) $opt['map_meta_cap'] = $this->mapMetaCap;
        if (isSet($this->hierarchical)) $opt['hierarchical'] = $this->hierarchical;
        if (isSet($this->supports)) $opt['supports'] = $this->supports;
        if (isSet($this->registerMetaBoxCb)) $opt['register_meta_box_cb'] = $this->registerMetaBoxCb;
        if (isSet($this->taxonomies)) $opt['taxonomies'] = $this->taxonomies;
        if (isSet($this->hasArchive)) $opt['has_archive'] = $this->hasArchive;
        if (isSet($this->rewrite)) $opt['rewrite'] = $this->rewrite;
        if (isSet($this->permalinkEpmask)) $opt['permalink_epmask'] = $this->permalinkEpmask;
        if (isSet($this->queryVar)) $opt['query_var'] = $this->queryVar;
        if (isSet($this->canExport)) $opt['can_export'] = $this->canExport;
        if (isSet($this->deleteWithUser)) $opt['delete_with_user'] = $this->deleteWithUser;
        if (isSet($this->showInRest)) $opt['show_in_rest'] = $this->showInRest;
        if (isSet($this->restBase)) $opt['rest_base'] = $this->restBase;
        if (isSet($this->restControllerClass)) $opt['rest_controller_class'] = $this->restControllerClass;

        return $opt;
    }

    private function labelOptions(): array
    {
        $labels = [];
        if (isSet($this->name)) $labels['name'] = $this->name;
        if (isSet($this->singularName)) $labels['singular_name'] = $this->singularName;
        if (isSet($this->addNew)) $labels['add_new'] = $this->addNew;
        if (isSet($this->addNewItem)) $labels['add_new_item'] = $this->addNewItem;
        if (isSet($this->editItem)) $labels['edit_item'] = $this->editItem;
        if (isSet($this->newItem)) $labels['new_item'] = $this->newItem;
        if (isSet($this->viewItem)) $labels['view_item'] = $this->viewItem;
        if (isSet($this->viewItems)) $labels['view_items'] = $this->viewItems;
        if (isSet($this->searchItems)) $labels['search_items'] = $this->searchItems;
        if (isSet($this->notFound)) $labels['not_found'] = $this->notFound;
        if (isSet($this->notFoundInTrash)) $labels['not_found_in_trash'] = $this->notFoundInTrash;
        if (isSet($this->parentItemColon)) $labels['parent_item_colon'] = $this->parentItemColon;
        if (isSet($this->allItems)) $labels['all_items'] = $this->allItems;
        if (isSet($this->archives)) $labels['archives'] = $this->archives;
        if (isSet($this->attributes)) $labels['attributes'] = $this->attributes;
        if (isSet($this->insertIntoItem)) $labels['insert_into_item'] = $this->insertIntoItem;
        if (isSet($this->uploadedToThisItem)) $labels['uploaded_to_this_item'] = $this->uploadedToThisItem;
        if (isSet($this->featuredImage)) $labels['featured_image'] = $this->featuredImage;
        if (isSet($this->setFeaturedImage)) $labels['set_featured_image'] = $this->setFeaturedImage;
        if (isSet($this->removeFeaturedImage)) $labels['remove_featured_image'] = $this->removeFeaturedImage;
        if (isSet($this->useFeaturedImage)) $labels['use_featured_image'] = $this->useFeaturedImage;
        if (isSet($this->menuName)) $labels['menu_name'] = $this->menuName;
        if (isSet($this->filterItemsList)) $labels['filter_items_list'] = $this->filterItemsList;
        if (isSet($this->itemsListNavigation)) $labels['items_list_navigation'] = $this->itemsListNavigation;
        if (isSet($this->itemsList)) $labels['items_list'] = $this->itemsList;
        if (isSet($this->nameAdminBarm)) $labels['name_admin_bar'] = $this->nameAdminBarm;

        return $labels;
    }
}