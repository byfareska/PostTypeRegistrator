<?php
/**
 * Created by PhpStorm.
 * User: eska
 * Date: 12.09.18
 * Time: 20:59
 */

namespace KarolSzarafinowski\PostTypeRegistrator;


class PostTaxonomyRegistator
{
    public function __construct()
    {
        add_action('init', '\\' . get_class($this) . '::register');
    }

    public static function register(): void
    {
        // extend from this class and use class RegisterTaxonomy here
    }
}