<?php
/**
 * Created by PhpStorm.
 * User: eska
 * Date: 12.09.18
 * Time: 21:02
 */

namespace KarolSzarafinowski\PostTypeRegistrator;


class RegisterTaxonomy
{

    private $taxonomy;

    private $objectType;

    private $params = [];

    public function __construct(string $taxonomyName, string $postType, array $params)
    {
        $this->taxonomy = $taxonomyName;
        $this->objectType = $postType;
        $this->params = $params;
    }

    public static function factory(string $taxonomyName, string $postType, array $params): RegisterTaxonomy
    {
        return new RegisterTaxonomy($taxonomyName, $postType, $params);
    }

    public function register(): void
    {
        register_taxonomy(
            $this->taxonomy,
            $this->objectType,
            $this->params
        );
    }
}