<?php
/**
 * Created by PhpStorm.
 * User: eska
 * Date: 12.09.18
 * Time: 20:32
 */

namespace KarolSzarafinowski\PostTypeRegistrator;

class PostTypeRegistrator
{
    public function __construct()
    {
        add_action('init', '\\' . get_class($this) . '::register');
    }

    public static function register(): void
    {
        // extend from this class and use class Register here
    }

}